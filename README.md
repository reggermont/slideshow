# slideshow - Description en Français

## Sommaire

* [Fonctionnalités](#fonctionnalités)
* [Motivation](#motivation)
* [Requires](#requires)
* [Installation](#installation)
* [Contributors](#contributors)
* [License](#license)

## Fonctionnalités

### Développé :
* Affiche un message si le JS n'est pas activé
* Responsive (presque) et BEAU
* Appel des données en AJAX (format JSON)
* Administrable via une liste de variables dans le fichier configSlideshow.js
* Animation de l'affichage des titres et descriptions différents de l'animation des images
* Adaptation des images selon leurs dimensions
* Hover Play Pause
* Play, Pause on click

### En développement :
* Boutons Suivant, Précédent
* Module de controle : (Puces ou vignettes)

### En projet de rajout :
* Barre de la progression du temps d'attente selon le temps restant avant slide
* Description affichée si souris sur l'image avec animation
* Hover Play Pause avec une animation côté utilisateur (sera implémenté avec le bouton play pause)
* Hover Play Pause et Bouton Play Pause configurable / activable / désactivable

[Retour au sommaire](#sommaire)
## Motivation

Projet réalisé dans le cadre des études à l'ESGI

[Retour au sommaire](#sommaire)
## Requires

* jQuery (fourni si besoin)

[Retour au sommaire](#sommaire)
## Installation

* Importez dans votre projet le fichier js/scriptSlideshow
* Si vous n'avez pas jQuery, importez dans votre projet également le fichier js/jQuery-3.2.1.min.js ou appelez-le via une source en ligne
* Dans votre page HTML, insérez les codes suivants :
    * à l'endroit où vous voulez voir le slideshow, mettez le code suivant : `<div id="slideshow"></div>`. À noter : il est possible de renommer l'id. Pour cela, modifiez l'id du div et la variable "slideshowName" dans le fichier "configSlideshow.js"
    * à la fin de votre page HTML (la même), ajouter les codes suivants dans l'ordre : `<script src="PATH/configSlideshow.js"></script>` `<script src="PATH/scriptSlideshow.js"></script>`,
    "PATH" correspondant à l'arborescence du fichier "scriptSlideshow.js" (par défaut : "js")

[Retour au sommaire](#sommaire)
## Contributors

* [Romain Eggermont]
* [Samuel Antunes]

[Retour au sommaire](#sommaire)
## License

Ce projet est Open Source et accessible sur ce Github

[Retour au sommaire](#sommaire)

[Romain Eggermont]: <https://github.com/J-Ramsey>
[Samuel Antunes]: <https://github.com/NeverTwice>