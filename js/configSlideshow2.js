/* CONFIG FILES FOR SLIDESHOW
Modify values from vars can alterate good functioning of the slideshow.
Do it if you know what you're doing !
*/ 

/* 
----- EDITABLE JS VALUES -----
Used for JS use and more.
*/

// url of JSON datas of images to import.
var urlImport = "https://www.skrzypczyk.fr/slideshow.php";

// If changes are made in the JSON, just need to modify values here
var jsonValues = {
	url:"url",
	title:"title",
	description:"desc"
}

// Speed of the sliding animation (in ms)
var slidingSpeed = 1000;

// Time between the beginning of 2 sliding animations (MUST BE greater than slidingSpeed) (in ms)
var timeBetweenAnimations = 3000;

// Type of animation for the title. For the moment, no values available
var animatingTitleType = "";

// Type of animation for the description. For the moment, no values available
var animatingDescriptionType = "fade";

// Thumbnail style (classic, modern)
var thumbnailStyle = "classic";

// if true, force play or force pause on click on Play / Pause Button. Events with mouse enter and mouse leave will not trigger
var forceOnClick = false;

// if true, hover the image pause it. leave the image with the mouse to play again the slideshow.
var hoverPause = false; 

// if true, enable play pause buttons
var enablePlayPauseButtons = true;

// if true, enable previous and next buttons
var enableLeftRightButtons = true;

// if true, enables thumbnail controls
var showControls = true;

// if true, enables title 
var showTitle = false;

// if true, enables description 
var showDescription = false;


/* 
----- HTML VALUES -----
Used to change the HTML and more
*/

// Change play button design (font awesome icon only)
var playDesign = "fa-play";

// Change pause button design (font awesome icon only)
var pauseDesign = "fa-pause";

// Change previous button design (font awesome icon only)
var previousDesign = "fa-chevron-left"

// Change next button design (font awesome icon only)
var nextDesign = "fa-chevron-right"

// Manage play/pause color (hexa only)
var playPauseColor = "rgba(0,0,0,0.5)";

// Play / Pause color when clicked with forceOnClick activated
var playPauseColorActivated = "rgba(255,255,255, 0.2)";

// Glow of pause play button (rgba only)
var playPauseGlowColor = "rgba(255,255,255, 0.2)"

// Thumbnail selected circle color
var selectedCircleColor = "black";

// Thumbnail unselected circle color
var unselectedCircleColor = "black";

// Font for buttons
var fontFamily = "calibri";

// Button background-color
var buttonBackground = "#3ea0cc";

// Button hover - background-color
var buttonHoverBackground = "#cacaca";

// Button hover - background-color
var onImageBackground = " rgba(255,255,255,0.3) ";

// Button hover - background-color
var onImageButtons = " white ";

// Button text-color
var buttonTextColor = "white";

// Name of the id of the div which will welcome the slideshow (need to be implemented in the code and to be the only one (of course))
var slideshowName = "slideshow";

// Different names used for ids and class. If already used in your html page, feel free to change them.
var titleName = "title";
var railName = "rail";
var viewedPartName = "viewed-part";
var itemsName = "items";
var descriptionName = "description";
var controlsName = "controls";
var controlersName = "controllers";
var playButton = "playButton";
var pauseButton = "pauseButton";
var previousButton = "previousButton";
var nextButton = "nextButton";
/* 
----- EDITABLE CSS VALUES -----
Used to change the CSS and more. 
*/

// width of the slideshow
var slideshowWidth = 100;

// width of viewed-part class
var viewedPartWidth = 60;

// max-height for image (in percent of the screen)
var imageMaxHeight = 80;

// For Media Queries, max-width for mobile display
var mediaWidth = 1023;

// true to center slideshow, false to left-align
var centered = true;

// full parameters for play / pause contour
var fullPlayPauseColor = "text-shadow: -1px 0 " + playPauseGlowColor + ", 0 1px " + playPauseGlowColor + ", 1px 0 " + playPauseGlowColor + ", 0 -1px " + playPauseGlowColor;

/* 
----- NON-EDITABLE JS VALUES -----
Used for JS use and more. Don't modify from this line to the end of the file
*/

// All images from external link
var images = [];

// number of images loaded
var numberOfImages = 0;

// position of slideshow
var position = 1;

// true if mouse over slideshow, false if not
var slideshowPaused = false;

// true if mouse over slideshow, false if not
var slideshowHovered = false;

// true if force play activated, false if not
var playClicked = false;

// true if force pause activated, false if not
var pauseClicked = false;

// Counter for description slide
var counterDescription = 1;

// timer
var timer;

// User clicked on next ?
var doingNext = false;

// User clicked on previous ?
var doingPrevious = false;

// Is Slideshow animating ?
var animating = false;