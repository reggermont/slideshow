$(document).ready(function() {

    // Synchronous (sorry) AJAX Function to get JSON datas of images
    $.ajax({
       async: false,
       url :  urlImport,
       type : 'GET',
       dataType : 'json',
       processData: false,
       success : function(data){  
           // Allowed due to async false
           images = data;
       }
    });

    var nbImages = images.length; 
    var pattern = "";

    for(var i in images){
        // Concat of pattern for each image from json
        pattern += 
            "<div id='image_"+i+"' class='" + itemsName + "' style='background-image : url(" + images[i][jsonValues['url']] + ");' > \
            </div>";
        numberOfImages++;
    }

    initialiseAll(images);
    $("#" + railName).html(pattern);

        timer = setInterval(nextImage, timeBetweenAnimations);

    /* Action Observers */
    $("#" + railName).mouseenter(function(){
        slideshowHovered = true;
        pauseSliding();
    }).mouseleave(function(){
        slideshowHovered = false;
        playSliding();
    });

    $("#" + playButton).on("click", function() {
        slideshowPaused = false;
        playClicked = !playClicked;
        if(playClicked)
            playSliding();
        else
            pauseSliding();
        pauseClicked = false;
        actualisePlayPauseColors();
    });

    $("#" + pauseButton).on("click", function() {
        slideshowPaused = true;
        pauseClicked = !pauseClicked;
        playClicked = false;
        actualisePlayPauseColors();
    });

    $("#" + railName).click(function() {
        changeSlidingStatus();
    });

    
    $("."+controlersName).click(function() {
        if(thumbnailStyle == 'classic') {
            $("."+controlersName).each(function() {
            if($(this).hasClass('fa-circle')) {
                $(this).removeClass('fa-circle');
                $(this).addClass('fa-circle-o');
            }
            });
            $(this).removeClass('fa-circle-o');
            $(this).addClass('fa-circle');
            position = $(this).attr('id').substr(11,1);
            clearInterval(timer);
            if(slideshowPaused) {
                slideshowPaused = false;
            }
            nextImage();
            if(!slideshowPaused && pauseClicked) {
                slideshowPaused = true;            
            }
            timer = setInterval(nextImage, timeBetweenAnimations);
        } else if(thumbnailStyle == "modern") {
            $("."+controlersName).each(function() {
            if($(this).hasClass('show_control')) {
                $(this).removeClass('show_control');
            }
            });
            $(this).addClass('show_control');
            position = $(this).attr('id').substr(11,1);
            clearInterval(timer);
            if(slideshowPaused) {
                slideshowPaused = false;
            }
            nextImage();
            if(!slideshowPaused && pauseClicked) {
                slideshowPaused = true;            
            }
            timer = setInterval(nextImage, timeBetweenAnimations);
        }
    });

    $('#' + previousButton).on("click", function() {
        if(!animating) {
            animating = true;
            forcePreviousImage();
            setTimeout(function(){animating = false},slidingSpeed);
        }
    });

    $('#' + nextButton).on("click", function() {
        if(!animating) {
            animating = true;
            forceNextImage();
            setTimeout(function(){animating = false},slidingSpeed);
        }
    });
});

// Initialise HTML and CSS content
function initialiseAll(images)
{
    var count = 0;
    var htmlContent = '';
    if(showTitle) 
        htmlContent = ' <div class="title_block_'+thumbnailStyle+'"><h3 id="' + titleName + '"></h3></div>';
                    htmlContent += '<div id="' + viewedPartName + '"> ';
                    if(enableLeftRightButtons)
                        htmlContent += '<div id="' + previousButton + '" class="left_right_'+thumbnailStyle+' fa ' + previousDesign + '"> </div>';
                        htmlContent += ' <div id="' + railName + '"></div>';
                        if(enablePlayPauseButtons)
                            htmlContent += '<div id="' + pauseButton + '" class="fa ' + pauseDesign + '"> </div>\
                                            <div id="' + playButton + '" class="fa ' + playDesign + '"> </div>';
                            if(enableLeftRightButtons)
                                htmlContent += '<div id="' + nextButton + '" class="left_right_'+thumbnailStyle+' fa ' + nextDesign + '"> </div> \
                            </div>';
                        if(showControls) {
                            htmlContent+= '<div class="' + controlsName + '"> ';
                            for(var i in images) {
                                if(thumbnailStyle == 'classic') {
                                    if(!count)
                                        htmlContent += '<i id="controller_'+i+'" class=" '+controlersName+' fa fa-circle" aria-hidden="true"></i>';
                                    else
                                        htmlContent += '<i id="controller_'+i+'" class=" '+controlersName+' fa fa-circle-o" aria-hidden="true"></i>';
                                }
                                if(thumbnailStyle == 'modern') {
                                    if(!count)
                                        htmlContent += '<div id="controller_'+i+'" class="show_control '+controlersName+' '+thumbnailStyle+'"><span class="nb_controllers">'+(count+1)+'</span></div>';
                                    else
                                        htmlContent += '<div id="controller_'+i+'" class=" '+controlersName+' '+thumbnailStyle+'"><span class="nb_controllers">'+(count+1)+'</span></div>';
                                }
                                count++;
                            }
                            htmlContent += '</div>';
                        }
                    htmlContent += '</div>\
                    <div class="block_desc"></div>';
                    if(showDescription) 
                        htmlContent+= '<div class="description_'+thumbnailStyle+'" id="' + descriptionName + '"></div>';
        
    $("#" + slideshowName).html(htmlContent);


    /**
     * Here is the main CSS content for the JS Slideshow, feel free to change it in order to match your needs according to the classes and ids you chose
     */
    var cssText = "";
    
    cssText += "<style >";

    cssText += "body { margin: 0; padding: 0;}";

    cssText += ".fa-circle {color: "+selectedCircleColor+"; }";

    cssText += ".fa-circle-o {color: "+unselectedCircleColor+"; }";
    
    cssText += "#" + slideshowName + "{ width: " + slideshowWidth + "%; text-align:center; } "; // Center everything in the slideshow
    
    cssText += "#" + slideshowName + ", #" + slideshowName + " * { margin:0; padding:0; } "; // Avoid every custom padding and margin in the slideshow
    
    var margin = '';
    if(thumbnailStyle == 'modern') {
        margin = 'margin-left:20% !important;';
    }

    cssText += "#" + viewedPartName + "{ width: " + viewedPartWidth + "%; overflow: hidden; position:relative; "+margin+" "; // All images are side by side. This allow to hide all other images and only keep one.
    if(centered)
        cssText += "margin-left: auto;margin-right: auto"; // If centered is true : the slideshow is centered.
    cssText += " } ";


    cssText += "#" + railName + " img { max-width: 100%; } "; // Allow images to take all width possible without overflow. max-width parameter is for small images.
    
    cssText += "#" + railName + " { width: " + numberOfImages + "00%; height: " + imageMaxHeight +"vh ; } "; // number of images * 100%. 
    
    cssText += "." + itemsName + "{ width : " + (100/numberOfImages) + "% ; display : inline-block ; height: " + imageMaxHeight +"vh ; background-position: center center;  background-size: cover;} "; // all items (div of a image) take 100% of the space possible, so we need to divide the 100% by the number of images.
    
    cssText += "." + controlersName+ " { margin-left: 15px  !important; margin-right: 15px  !important; }";

    cssText += "." + controlersName+ ": first-child { margin-left: 0px !important; margin-right: 0px  !important; }";

    cssText += "." + controlersName+ ": lastt-child {margin-left: 0px  !important; margin-left: 0px  !important; }";

    cssText += "#" + pauseButton + "{ position:absolute ; left:10px ; bottom:10px; font-size:1.5em ; color : " + playPauseColor + " ; }" // Fix play button at bottom left and give him colors

    cssText += "#" + playButton + "{ position:absolute ; left:50px ; bottom:10px; font-size:1.5em ; color : " + playPauseColor + " ; }" // Fix pause button at bottom left and give him colors

    cssText += ".modern:hover {background-color: "+buttonHoverBackground+"; cursor:pointer;}";

    cssText += ".modern { display:inline-block; height:40px; margin-left:5px !important; margin-right:5px  !important}";
    
    cssText += ".show_control {background-color: "+ buttonBackground+ "; color:"+buttonTextColor+"}";
   
    cssText += "." + controlsName + "{margin-top:5px !important;}";

    if(imageMaxHeight == 100) {
        cssText += "#"+titleName+" {position: absolute;z-index: 1;color: black;top: 65px;left: 113px;font-size: 40px; font-family:"+fontFamily+";}";
        cssText += "#"+descriptionName+" {position: absolute;z-index: 1;color: black;top: 100px;left: 150px;font-size: 30px; font-family:"+fontFamily+";}";
    }
    cssText += ".left_right_modern {background: "+onImageBackground+";padding: 13px !important;color:"+onImageButtons+"}";

    cssText += ".left_right_classic {font-size:2em!important;color:black;}";

    cssText += ".description_modern {position: relative;bottom: 100px;font-family:"+fontFamily+"; background: "+onImageBackground+";display: inline-block;padding: 15px !important;}";

    cssText += ".left_right_modern:hover {cursor: pointer;}";

    cssText += ".title_block_modern {font-family:"+fontFamily+";float:left; color:"+buttonTextColor+";position: relative; background: "+buttonBackground+";transform:skewX(15deg); -ms-transform:skewX(15deg); -webkit-transform:skewX(15deg); top: 25px;display: inline-block;padding: 10px!important;padding-right: 40px !important;padding-left: 30px !important; z-index:1;left:19%}";
    
    cssText += "#"+titleName+" {transform:skewX(-15deg); -ms-transform:skewX(-15deg); -webkit-transform:skewX(-15deg);text-transform:uppercase;}";

    cssText += ".nb_controllers {padding:16px !important;top: 11px;position:relative;font-family:"+fontFamily+", cursive; }";

    cssText += "@-webkit-keyframes progress { 0% { width: 0%; } 100% { width: 100%; } }";

    cssText += "#" + previousButton + "{ position:absolute ; left:0px; font-size:1em; top:" + ((imageMaxHeight / 2) - 5) + "vh}";  // Fix previous button at center left

    cssText += "#" + nextButton + "{ position:absolute ; right:0px; font-size:1em; top:" + ((imageMaxHeight / 2 ) - 5) + "vh}";  // Fix previous button at center right

    cssText += "</style>"; // END OF CSS

    $("head").append(cssText);

    $("#" + titleName).html(images[(position-1)][jsonValues["title"]]);

    $("#" + descriptionName).html(images[(position-1)][jsonValues["description"]]);
}

// Move the rail with animation
function nextImage() 
{
    if(!slideshowPaused) {  
        position++;
        
        if(position > numberOfImages) {
            $("#" + railName).stop().animate({marginLeft: '0%'}, slidingSpeed);
            position = 1;
        }
        else {    
            $("#" + railName).stop().animate({marginLeft: '-'+ (position-1) +'00%'}, slidingSpeed);
        }

        animateText(titleName, animatingTitleType);
        animateText(descriptionName, animatingDescriptionType);

        nextThumbnail(position-1);
    }
}

// Animate to the previous image 
function previousImage()
{
    position--;
    if(position < 1) {
        $("#" + railName).animate({marginLeft: '-' + (numberOfImages - 1) + '00%'}, slidingSpeed);
        position = numberOfImages;
    }
    else
        $("#" + railName).animate({marginLeft: '-' + (position-1) +'00%'}, slidingSpeed);

    animateText(titleName, animatingTitleType);
    animateText(descriptionName, animatingDescriptionType);

    nextThumbnail(position - 1);
}

/* Animate text
 * content : Title or Description
 * type : Type of animation  
 */
function animateText(content, type)
{
    var typeOfContent;

    if(content == titleName)
        typeOfContent = jsonValues["title"];
    else
        typeOfContent = jsonValues["description"];

    switch(type) {

        case ("slideUp") : // Slide up / Slide down animation
            $("#" + content).slideUp((slidingSpeed/2), function(){
                $("#" + content).html(images[(position-1)][typeOfContent]);
                $("#" + content).slideDown((slidingSpeed/2));
            });
            break;

        case ("fade") : // Fade in / Fade out animation
            $("#" + content).fadeOut((slidingSpeed/2), function(){
                $("#" + content).html(images[(position-1)][typeOfContent]);
                $("#" + content).fadeIn((slidingSpeed/2));
            });
            break;

        default : // Default is Fade in / Fade Out
            $("#" + content).html(images[(position-1)][typeOfContent]);
            break;

    }
}

/* Change thumbnail when changing slide. Find the current active thumbnail to reinitialize it and active the new one 
 * position : new position of slider
 */
function nextThumbnail(position)
{
    if(thumbnailStyle == 'classic') {
        $('.controllers').each(function() {
        if($(this).hasClass('fa-circle')) {
                $(this).removeClass('fa-circle');
                $(this).addClass('fa-circle-o');
            }
        });
        if($("#controller_"+(position)+"").hasClass('fa-circle-o')) {
            $("#controller_"+(position)+"").removeClass('fa-circle-o');
            $("#controller_"+(position)+"").addClass('fa-circle');
        }  
    } else if(thumbnailStyle == 'modern') {
        $('.controllers').each(function() {
        if($(this).hasClass('show_control')) {
                $(this).removeClass('show_control');
            }
        });
        $("#controller_"+(position)+"").addClass('show_control');
    }
}

// Invert status of sliding
function changeSlidingStatus()
{
    if(!slideshowPaused)
        pauseSliding();
    else
        playSliding();
}

// Pause sliding
function pauseSliding()
{
    if(!pauseClicked && !playClicked)
        slideshowPaused = true;
}

// Play sliding
function playSliding()
{
    if(!pauseClicked && !playClicked)
        slideshowPaused = false;
}

// Checked status of play and pause buttons to change their colors
function actualisePlayPauseColors()
{
    if(playClicked)
        $("#" + playButton).css("color", playPauseColorActivated);
    else
        $("#" + playButton).css("color", playPauseColor);
    if(pauseClicked)
        $("#" + pauseButton).css("color", playPauseColorActivated);
    else
        $("#" + pauseButton).css("color", playPauseColor);
}

// Force the next image if user click on next button
function forceNextImage()
{
    slideshowPaused = false;
    nextImage();
    slideshowPaused = true;
    reloadInterval();
}

// Force the previous image if user click on previous button
function forcePreviousImage()
{
    previousImage();
    reloadInterval();
}

// Reload the interval
function reloadInterval()
{
    clearInterval(timer)
    timer = setInterval(nextImage, timeBetweenAnimations);
}